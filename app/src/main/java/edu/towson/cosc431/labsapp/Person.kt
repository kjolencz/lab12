package edu.towson.cosc431.labsapp

import com.google.gson.annotations.SerializedName
data class Person(
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("last_name")
    val lastName: String,
    @SerializedName("icon_url")
    val iconUrl: String)
