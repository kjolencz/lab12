package edu.towson.cosc431.labsapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.person_item.view.*

class PersonAdapter(val controller: IController): RecyclerView.Adapter<PersonViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.person_item, parent, false)

        return PersonViewHolder(view)
    }

    override fun getItemCount(): Int {
        return controller.people.size
    }

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        val person = controller.people.get(position)
        holder.itemView.first_name.text = person.firstName
        holder.itemView.last_name.text = person.lastName
        holder.itemView.icon.visibility = View.INVISIBLE
        holder.itemView.loader.visibility = View.VISIBLE

        controller.getIcon(person.iconUrl, callback = {icon ->
            if(holder.adapterPosition == position){
                holder.itemView.loader.visibility = View.INVISIBLE
                holder.itemView.icon.setImageBitmap(icon)
                holder.itemView.icon.visibility = View.VISIBLE
            }
        })
    }
}

class PersonViewHolder(view: View): RecyclerView.ViewHolder(view)